//
//  GameScene.swift
//  FinalExamIOS
//
//  Created by Sukhwinder Rana on 2019-06-19.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var leg: SKNode?
    var hair1:SKNode?
    var hair2:SKNode?
    var hair3:SKNode?
    var hair4:SKNode?
    var timePassed:TimeInterval?
    var messageLabel:SKLabelNode!
    
    
    override func didMove(to view: SKView) {
        
        // Get label node from scene and store it for use later
         self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
       
        self.hair1 = self.childNode(withName: "hair1")
        self.hair2 = self.childNode(withName: "hair1")
        self.hair3 = self.childNode(withName: "hair1")
        self.hair4 = self.childNode(withName: "hair1")
         self.leg = self.childNode(withName: "leg")
        self.messageLabel = SKLabelNode(text: "Time: \(timePassed)")

        // self.messageLabel = SKLabelNode(text: "Time: \(timePassed)")
        messageLabel.fontColor = UIColor.yellow
        messageLabel.fontSize = 60
        messageLabel.zPosition = 99
        messageLabel.position.x = 200
        messageLabel.position.y = 500
        addChild(messageLabel)
        
       
        
        
    }
    var timeOfLastUpdate:TimeInterval = -1;
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if (timeOfLastUpdate == -1) {
            // First time, set timeOfLastUpdate
            // to current time
            timeOfLastUpdate = currentTime
        }
        
         timePassed = currentTime - timeOfLastUpdate
        messageLabel.text = "Time: \(timePassed)"
       
        
        
        if (timePassed! >= 10) {
            print("20 SECONDS PASSED!")
            timeOfLastUpdate = currentTime
        
                let level2Scene = SKScene(fileNamed:"Lemming")
                level2Scene!.scaleMode = .aspectFill
                let flipTransition = SKTransition.flipVertical(withDuration: 2)
                self.scene?.view?.presentScene(
                    level2Scene!,
                    transition: flipTransition)
                
                //            if let scene = SKScene(fileNamed: "Level02") {
                //                // Set the scale mode to scale to fit the window
                //                scene.scaleMode = .aspectFill
                //
                //                // Present the scene
                //                scene.view?.presentScene(scene)
                //            }
         

                
        }
        
    }
       var mouseStartingPosition:CGPoint = CGPoint(x:0, y:0)
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let mousePosition = touch.location(in:self)
        
        // 2. if person clicked tree, then
        // make an orange
        
        // 2a. What did the person click?
        let spriteTouched = self.atPoint(mousePosition)
        
        // 2b. If person click tree, then make orange
        if (spriteTouched.name == "hair1") {
            self.mouseStartingPosition = mousePosition
            print("\(spriteTouched.name)")
            while(spriteTouched.name != "hair2"){
                print("\(spriteTouched.name)")
               return
            }
            while(spriteTouched.name != "hair3"){
                print("\(spriteTouched.name)")
               return
            }
            while(spriteTouched.name == "hair4"){
                print("\(spriteTouched.name)")
                break
            }
            let messageLabel = SKLabelNode(text: "you win")
            messageLabel.fontColor = UIColor.yellow
            messageLabel.fontSize = 60
            messageLabel.position.x = self.size.width/2
            messageLabel.position.y = self.size.height/2
            addChild(messageLabel)
            var i = 0
            while (i <= 1000){
                i = i + 1
                }
//            hair1?.physicsBody?.affectedByGravity = true
//             hair1?.physicsBody?.isDynamic = true
            
            
//                while(spriteTouched.name != "hair2"){
//                    if(timePassed! >= 20 ){
//                        break
//                    }
//                }
           // hair2?.physicsBody?.affectedByGravity = true
           // hair2?.physicsBody?.isDynamic = true
            
        
            
        }
    }
}
