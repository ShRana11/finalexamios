//
//  Lemming.swift
//  FinalExamIOS
//
//  Created by Sukhwinder Rana on 2019-06-19.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import Foundation

import SpriteKit
import GameplayKit


class Lemming: SKScene, SKPhysicsContactDelegate {
    
    // sprites
   // var player:SKNode!
    var enemiesArray:[SKNode] = []
   // var evilOtto:SKNode!
    var exit:SKNode!
    var entry:SKNode!
    var ground:SKNode!
    var number = 0
    var spawn = true
    var messageLabel:SKLabelNode!
    
    
    
    
    override func didMove(to view: SKView) {
        // Required for SKPhysicsContactDelegate
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.physicsWorld.contactDelegate = self
        self.anchorPoint.x = 0
        self.anchorPoint.y = 0
        
        // get the sprites from SceneKit Editor
      //  self.player = self.childNode(withName: "player")
        self.entry = self.childNode(withName: "entry")
        self.exit = self.childNode(withName: "exit")
        self.ground = self.childNode(withName: "groung")
        
        if(spawn == true){
        let generateEnemySequence = SKAction.sequence(
                        [
                            SKAction.run {
                               [weak self] in self?.spawnEnemy()
                            },
                            SKAction.wait(forDuration: 2)
                        ]
                    )
                    self.run(SKAction.repeatForever(generateEnemySequence))


        }
    }
    
    // MARK: Function to notify us when two sprites touch
    func didBegin(_ contact: SKPhysicsContact) {
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
         //print("Player reached exit")
        print("\(nodeA!.name), \(nodeB!.name)")
      //  var i = "Optional("nil")"
        
        if (nodeA!.name == nil || nodeB!.name == nil  ){
            if(nodeB?.name == "exit" || nodeA!.name == "exit"){
            print("Player reached exit")
            number = number + 1
                removeFromParent()
                removeChildren(in: [enemiesArray.remove(at: 0)])
            
            
            // GAME WIN!
            if(number >= 13){
            messageLabel = SKLabelNode(text: "\(number) lennings exit")
            messageLabel.fontColor = UIColor.yellow
            messageLabel.fontSize = 60
            messageLabel.position.x = self.size.width * 0.60
            messageLabel.position.y = 10
            addChild(messageLabel)
            
            }
        }
 
        
    }
    }
    
    var timeOfLastUpdate:TimeInterval = -1;
   var enemyMovingLeft:Bool = false
    
    override func update(_ currentTime: TimeInterval) {
        
        for (index , enemy) in enemiesArray.enumerated(){
            if(enemyMovingLeft == false){
            enemy.position.x = enemy.position.x + 5
              //  print("right:\(enemy.position.x)")
            }
            if(enemyMovingLeft == true){
                 enemy.position.x = enemy.position.x - 5
                //print("left:\(enemy.position.x)")
                
            }
            
            if(enemy.position.x >= self.size.width - 100){
                enemyMovingLeft = true
            }
            else if(enemy.position.x <= 90){
                enemyMovingLeft = false
                
            }
            
            
            
        }
        // MARK: Deal with Evil Otto
        // -----------------------------
        // this code handles the first time you call the update() function
        if (timeOfLastUpdate == -1) {
            // First time, set timeOfLastUpdate
            // to current time
            timeOfLastUpdate = currentTime
        }
        
        let timePassed = currentTime - timeOfLastUpdate
        if(number >= 13 && timePassed <= 30){
            self.messageLabel.text = "You Win"
            let level2Scene = SKScene(fileNamed:"GameScene")
            level2Scene!.scaleMode = .aspectFill
            let flipTransition = SKTransition.flipVertical(withDuration: 2)
            self.scene?.view?.presentScene(
                level2Scene!,
                transition: flipTransition)
        }
        
        else if (timePassed >= 30) {
            print("30 SECONDS PASSED!")
            timeOfLastUpdate = currentTime
             self.messageLabel.text = "You Loss"
            let level2Scene = SKScene(fileNamed:"GameScene")
            level2Scene!.scaleMode = .aspectFill
            let flipTransition = SKTransition.flipVertical(withDuration: 2)
            self.scene?.view?.presentScene(
                level2Scene!,
                transition: flipTransition)
            
        }
        // ------ END TIME DETECTION -------------
        
        // MOVE EVIL OTTO
//        if (self.evilOttoIsMoving == true) {
//            evilOtto.position.x = evilOtto.position.x - 10
//        }
        
        
    }
    func spawnEnemy() {
        // 1. create a sand node
        if(enemiesArray.count <= 14){
        let lemming = SKSpriteNode(imageNamed: "rat")
        
        // 2. set the initial position of the sand
        lemming.position.x = self.entry.position.x      //  x = w/2
        lemming.position.y = self.entry.position.y  // y = h-100
        
        // 3. add physics body to sand
        //lemming.physicsBody = SKPhysicsBody(circleOfRadius:sandParticle.size.width/2)
        let BodySize = CGSize(width: lemming.size.width, height: lemming.size.height)
        self.enemiesArray.append(lemming)

        lemming.physicsBody = SKPhysicsBody(rectangleOf: BodySize)
        
        addChild(lemming)
        }
        else{
            self.spawn = false
           // return
        }
       
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first!
        let mousePosition = touch.location(in:self)
        
        
        let spriteTouched = self.atPoint(mousePosition)
        
        let base = SKSpriteNode(imageNamed: "grass")
        base.position.x = mousePosition.x
        base.position.y = mousePosition.y
        
    
        print("x:\(spriteTouched.name)")
        let BodySize = CGSize(width: base.size.width, height: base.size.height)
        base.physicsBody =
            SKPhysicsBody(rectangleOf: BodySize)
         addChild(base)
         base.physicsBody?.isDynamic = false
        
//         base.physicsBody?.affectedByGravity = false
//
    
    }

    
}


